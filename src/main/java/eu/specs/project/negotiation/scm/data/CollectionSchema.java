package eu.specs.project.negotiation.scm.data;

import java.util.List;

public class CollectionSchema {
    private String resource;
    private Integer total;
    private Integer members;
    private List<String> item_list;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }

    public List<String> getItem_list() {
        return item_list;
    }

    public void setItem_list(List<String> item_list) {
        this.item_list = item_list;
    }
}
