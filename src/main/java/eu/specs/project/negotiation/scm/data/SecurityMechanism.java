package eu.specs.project.negotiation.scm.data;

import java.util.List;

public class SecurityMechanism {
    protected String id;
    protected String created;
    protected String updated;
    protected String name;
    protected List<String> securityCapabilities;
    protected List<String> enforcedMetrics;
    protected List<String> monitoredMetrics;
    protected List<String> resources;
    protected String metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSecurityCapabilities() {
        return securityCapabilities;
    }

    public void setSecurityCapabilities(List<String> securityCapabilities) {
        this.securityCapabilities = securityCapabilities;
    }

    public List<String> getEnforcedMetrics() {
        return enforcedMetrics;
    }

    public void setEnforcedMetrics(List<String> enforcedMetrics) {
        this.enforcedMetrics = enforcedMetrics;
    }

    public List<String> getMonitoredMetrics() {
        return monitoredMetrics;
    }

    public void setMonitoredMetrics(List<String> monitoredMetrics) {
        this.monitoredMetrics = monitoredMetrics;
    }

    public List<String> getResources() {
        return resources;
    }

    public void setResources(List<String> resources) {
        this.resources = resources;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
}
