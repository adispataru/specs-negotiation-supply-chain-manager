package eu.specs.project.negotiation.scm;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.enforcement.*;
import eu.specs.datamodel.metrics.AbstractMetricType;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.project.negotiation.scm.data.CollectionSchema;
import eu.specs.project.negotiation.scm.utils.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SupplyChainManager {
    private static final Logger logger = LogManager.getLogger(SupplyChainManager.class);
    private Client client;
    private String serviceManagerApiAddress;
    private String planningApiAddress;

    public SupplyChainManager(String serviceManagerApiAddress, String planningApiAddress) {
        this.serviceManagerApiAddress = serviceManagerApiAddress;
        this.planningApiAddress = planningApiAddress;

        client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                        //.register(new LoggingFilter())
                .build();
    }

    public List<SupplyChain> buildSupplyChains(AgreementOffer agreementOffer, String slatId) throws Exception {

        logger.trace("Building supply chains for agreement offer {} and SLAT {}.", agreementOffer.getName(), slatId);

        // extract metrics from the agreement offer and retrieve security mechanisms from the Service Manager
        List<SecurityMechanism> securityMechanisms = new ArrayList<>();
        List<CloudResources> cloudResourcesList = new ArrayList<>();

        ServiceDescriptionTerm serviceDescriptionTerm = null;
        for(Term t : agreementOffer.getTerms().getAll().getAll()){
            if(t instanceof ServiceDescriptionTerm)
                serviceDescriptionTerm = (ServiceDescriptionTerm) t;
        }
        List<AbstractMetricType> securityMetrics =
                serviceDescriptionTerm.getServiceDescription().getSecurityMetrics().getSecurityMetric();

        for (AbstractMetricType securityMetric : securityMetrics) {
            List<SecurityMechanism> securityMechanismsMetric =
                    retrieveSecurityMechanisms(securityMetric.getReferenceId());
            securityMechanisms.addAll(securityMechanismsMetric);
        }

        ServiceDescriptionType.ServiceResources serviceResources =
                serviceDescriptionTerm.getServiceDescription().getServiceResources().get(0);
        for (ServiceDescriptionType.ServiceResources.ResourcesProvider resourcesProvider : serviceResources.getResourcesProvider()) {
            CloudResources cloudResources = new CloudResources();
            cloudResourcesList.add(cloudResources);
            cloudResources.setProviderId(resourcesProvider.getId());
            CloudResources.Zone zone = new CloudResources.Zone();
            cloudResources.addZone(zone);
            zone.setZoneId(resourcesProvider.getZone());
            // TODO: resourcesProvider.getMaxAllowedVMs() should be integer?
            zone.setMaxAllowedVMs(Integer.parseInt(resourcesProvider.getMaxAllowedVMs()));
            for (ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm : resourcesProvider.getVM()) {
                CloudResources.VmType vmType = new CloudResources.VmType();
                vmType.setAppliance(vm.getAppliance());
                vmType.setHardware(vm.getHardware());
                zone.addVmType(vmType);
            }
        }

        // extract SLOs from agreement offer
        List<SLO> slos = new ArrayList<>();
        GuaranteeTerm guaranteeTerm = null;
        for(Term t : agreementOffer.getTerms().getAll().getAll()){
            if(t instanceof GuaranteeTerm)
                guaranteeTerm = (GuaranteeTerm) t;
        }
        for (SLOType sloType : guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO()) {
            SLO slo = new SLO();
            slos.add(slo);
            slo.setId(sloType.getSLOID());
            slo.setMetricId(sloType.getMetricREF());
            slo.setSloexpression(sloType.getSLOexpression());
            slo.setImportanceLevel(sloType.getImportanceWeight());
        }

        // TODO: build sets of mechanisms

        // prepare input data for supply chain activity creation
        SupplyChainActivity scaData = new SupplyChainActivity();
        scaData.setSlaId(slatId);
        scaData.setSecurityMechanisms(securityMechanisms);
        scaData.setCloudResources(cloudResourcesList);
        scaData.setSlos(slos);

        WebTarget planningApiTarget = client.target(planningApiAddress);
        logger.trace("Creating supply chain activity : sending request to {}", planningApiAddress);
        URI scaUri;
        try {
            Response response = planningApiTarget
                    .path("/sc-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(scaData), Response.class);

            scaUri = response.getLocation();

        } catch (Exception e) {
            throw new Exception("Failed to create supply chain activity: " + e.getMessage(), e);
        }

        logger.trace("Supply chain activity created successfully. URI: {}", scaUri);

        // wait until the supply chain activity is finished
        Date startTime = new Date();
        do {
            try {
                logger.trace("Retrieving supply chain activity state...");
                Thread.sleep(500);
                Map statusResponse = client
                        .target(UriBuilder.fromUri(scaUri).path("state"))
                        .request(MediaType.APPLICATION_JSON)
                        .get(Map.class);

                logger.trace("Supply chain activity state: {}", statusResponse.get("state"));
                if (statusResponse.get("state").equals(SupplyChainActivity.Status.COMPLETED.name()) ||
                        statusResponse.get("state").equals(SupplyChainActivity.Status.ERROR.name())) {
                    break;
                }
                if (new Date().getTime() - startTime.getTime() > 30000) {
                    // timeout
                    throw new Exception("Timeout waiting for supply chain activity process to finish.");
                }

            } catch (Exception e) {
                throw new Exception("Failed to retrieve supply chain activity state: " + e.getMessage(), e);
            }
        } while (true);

        logger.trace("Retrieving list of built supply chains...");
        List<SupplyChain> supplyChains;
        try {
            supplyChains = client
                    .target(UriBuilder.fromUri(scaUri).path("supply-chains"))
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<List<SupplyChain>>() {
                    });
        } catch (Exception e) {
            throw new Exception("Failed to retrieve built supply chains of the supply chain activity: " + e.getMessage(), e);
        }


        if (logger.isTraceEnabled()) {
            logger.trace("Built supply chains:\n{}", JsonDumper.dump(supplyChains));
        }

        return supplyChains;
    }

    private List<SecurityMechanism> retrieveSecurityMechanisms(String metricId) {
        // retrieve security mechanisms from Service manager for the specified metricId
        WebTarget serviceManagerTarget = client.target(serviceManagerApiAddress);
        logger.trace("Retrieving security mechanisms for metric '{}' from {}.",
                metricId, serviceManagerApiAddress);
        CollectionSchema securityMechanismsResponse = serviceManagerTarget
                .path("/security-mechanisms")
                .queryParam("metric_id", metricId)
                .request(MediaType.APPLICATION_JSON)
                .get(CollectionSchema.class);

        if (logger.isTraceEnabled()) {
            logger.trace("Response:\n{}", JsonDumper.dump(securityMechanismsResponse));
        }

        List<SecurityMechanism> securityMechanisms = new ArrayList<>();
        if (securityMechanismsResponse.getMembers() > 0) {
            // retrieve all security mechanisms
            for (String securityMechanismUri : securityMechanismsResponse.getItem_list()) {

                try {
                    logger.trace("Retrieving security mechanism {}.", securityMechanismUri);
                    eu.specs.project.negotiation.scm.data.SecurityMechanism securityMechanism1 = client
                            .target(securityMechanismUri)
                            .request(MediaType.APPLICATION_JSON)
                            .get(eu.specs.project.negotiation.scm.data.SecurityMechanism.class);
                    if (logger.isTraceEnabled()) {
                        logger.trace("Response:\n{}", JsonDumper.dump(securityMechanism1));
                    }

                    // TODO: SecurityMechanism returned from the Service Manager has different schema
                    SecurityMechanism securityMechanism = new SecurityMechanism();
                    securityMechanism.setName(securityMechanism1.getName());
                    securityMechanism.setSecurityCapabilities(securityMechanism1.getSecurityCapabilities());
                    securityMechanism.setEnforceableMetrics(securityMechanism1.getEnforcedMetrics());
                    securityMechanism.setMonitorableMetrics(securityMechanism1.getMonitoredMetrics());

                    securityMechanisms.add(securityMechanism);
                } catch (Exception e) {
                    logger.error("Failed to retrieve security mechanism {}: {}",
                            securityMechanismUri, e.getMessage());
                }
            }
        }

        return securityMechanisms;
    }
}
